import React, { Component } from "react";
import { View, Text, TouchableOpacity } from "react-native";
import * as MediaLibrary from "expo-media-library";
import { Camera } from "expo-camera";

export default class MyCam extends Component {
  state = {
    video: null,
    picture: null,
    recording: false,
  };

  _saveVideo = async () => {
    const { video } = this.state;
    const asset = await MediaLibrary.createAssetAsync(video.uri);
    console.log(asset, "after");
    this.props.navigation.navigate("RecVideo");
  };

  _StopRecord = async () => {
    this.setState({ recording: false }, () => {
      this.cam.stopRecording();
    });
  };

  _StartRecord = async () => {
    if (this.cam) {
      this.setState({ recording: true }, async () => {
        const video = await this.cam.recordAsync();
        this.setState({ video });
      });
    }
  };

  toogleRecord = () => {
    const { recording } = this.state;

    if (recording) {
      this._StopRecord();
    } else {
      this._StartRecord();
    }
  };

  render() {
    const { recording, video } = this.state;
    return (
      <Camera
        ref={(cam) => (this.cam = cam)}
        style={{
          justifyContent: "flex-end",
          alignItems: "center",
          flex: 1,
          width: "100%",
        }}
      >
        {video && (
          <TouchableOpacity
            onPress={this._saveVideo}
            style={{
              padding: 20,
              width: "100%",
              backgroundColor: "#ccc",
            }}
          >
            <Text style={{ textAlign: "center" }}>Save</Text>
          </TouchableOpacity>
        )}
        <TouchableOpacity
          onPress={this.toogleRecord}
          style={{
            padding: 20,
            width: "100%",
            backgroundColor: "#fff",
          }}
        >
          <Text style={{ textAlign: "center" }}>
            {recording ? "Stop" : "Record"}
          </Text>
        </TouchableOpacity>
      </Camera>
    );
  }
}

// if (asset) {
//   console.log(this.props.navigation.navigate);
//   this.setState({ video: null, recording: false, picture: null });
//   this.props.navigation.navigate("RecVideo");
// }
// setTimeout(() => {
//   this.setState({ cameraType: Camera.Constants.Type.front });
// }, 100);
// backgroundColor: recording ? "#ccc" : "#fff",
