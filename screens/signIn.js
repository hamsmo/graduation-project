import React from "react";
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Image,
  ImageBackground,
  TouchableWithoutFeedback,
  Keyboard,
  Alert,
} from "react-native";

import { Formik } from "formik";

import { globalStyles } from "../styles/style";
import Header from "../components/header";

export default function SignIn({ navigation }) {
  return (
    <TouchableWithoutFeedback
      onPress={() => {
        Keyboard.dismiss();
      }}
    >
      <ImageBackground
        source={require("../assets/background_img.jpg")}
        style={styles.container}
      >
        <Header />
        <Image
          source={require("../assets/signLanguage.png")}
          style={globalStyles.imageHeader}
        />
        <Text style={globalStyles.title}>Sign In</Text>
        <Formik
          initialValues={{ email: "", password: "" }}
          onSubmit={(values) => {
            if (values.email === "Admin" && values.password === "Admin") {
              navigation.navigate("RecVideo");
            } else {
              Alert.alert("OOPS!", "Wrong in email or password", [
                {
                  text: "OK",
                },
              ]);
            }
          }}
        >
          {(props) => (
            <View style={styles.container}>
              <TextInput
                style={globalStyles.input}
                placeholder="email"
                placeholderTextColor="#000"
                onChangeText={props.handleChange("email")}
                value={props.values.email}
              />
              <TextInput
                style={globalStyles.input}
                placeholder="password"
                placeholderTextColor="#000"
                onChangeText={props.handleChange("password")}
                value={props.values.password}
              />
              <View style={styles.DownPart}>
                <TouchableOpacity onPress={props.handleSubmit}>
                  <Text style={globalStyles.button}>Sign In</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => navigation.navigate("SignUp")}>
                  <Text style={styles.DontHave}>Don't have an account?</Text>
                </TouchableOpacity>
              </View>
            </View>
          )}
        </Formik>
      </ImageBackground>
    </TouchableWithoutFeedback>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    width: "100%",
  },
  DownPart: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    width: "100%",
    paddingHorizontal: 20,
  },
  DontHave: {
    marginTop: 12,
    textDecorationLine: "underline",
  },
});
