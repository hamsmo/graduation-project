import React from "react";
import { StyleSheet, Text, View, TouchableOpacity } from "react-native";

import { Entypo } from "@expo/vector-icons";

export default function SignIn({ navigation }) {
  return (
    <View style={styles.container}>
      <Text style={{ color: "#fff" }}>Press here to open Camera!</Text>
      <TouchableOpacity
        style={{ marginTop: 10 }}
        onPress={() => navigation.navigate("video")}
      >
        <Entypo name="camera" size={24} color="white" />
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#000",
  },
});
