import React, { Component } from "react";
import { View, Text, TouchableOpacity } from "react-native";
import * as Permissions from "expo-permissions";
import { AntDesign } from "@expo/vector-icons";

class RecVideo extends Component {
  state = {
    showCamera: false,
  };

  _showCamera = async () => {
    const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);

    if (status === "granted") {
      this.setState({ showCamera: true });
    }

    this.props.navigation.navigate("video");
  };

  render() {
    const { showCamera } = this.state;
    return (
      <View
        style={{
          justifyContent: "center",
          alignItems: "center",
          flex: 1,
          width: "100%",
          backgroundColor: "#000",
        }}
      >
        <TouchableOpacity onPress={this._showCamera}>
          <Text style={{ color: "#fff", fontSize: 20 }}> Record Video </Text>
          <AntDesign
            name="videocamera"
            size={20}
            color="white"
            style={{ textAlign: "center" }}
          />
        </TouchableOpacity>
      </View>
    );
  }
}

export default RecVideo;

// {
//   {showCamera ? (
//           // <MyCam navigation={this.props.navigation}/>
//           {this.props.navigation.navigate('video')}
//         ) : (
//   }
// }
