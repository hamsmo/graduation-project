import React from "react";
import {
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  Image,
  ImageBackground,
} from "react-native";

import { globalStyles } from "../styles/style";
import Header from "../components/header";

export default function SignIn() {
  return (
    <ImageBackground
      source={require("../assets/background_img.jpg")}
      style={styles.container}
    >
      <Header />
      <Image
        source={require("../assets/signLanguage.png")}
        style={globalStyles.imageHeader}
      />
      <Text style={globalStyles.title}>Sign Up</Text>
      <TextInput
        style={globalStyles.input}
        placeholder="enter your email"
        placeholderTextColor="#000"
      />
      <TextInput
        style={globalStyles.input}
        placeholder="password"
        placeholderTextColor="#000"
      />
      <TextInput
        style={globalStyles.input}
        placeholder="confirm password"
        placeholderTextColor="#000"
      />
      <TouchableOpacity>
        <Text style={globalStyles.button}>Sign Up</Text>
      </TouchableOpacity>
    </ImageBackground>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
  },
});
