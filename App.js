import React from "react";

import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";

import SignIn from "./screens/signIn";
import SignUp from "./screens/signUp";
import video from "./screens/video";
import RecVideo from "./screens/RecVideo";

const Stack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator
        screenOptions={{
          headerShown: false,
        }}
        initialRouteName="SignIn"
      >
        <Stack.Screen name="SignIn" component={SignIn} />
        <Stack.Screen name="SignUp" component={SignUp} />
        <Stack.Screen name="RecVideo" component={RecVideo} />
        <Stack.Screen name="video" component={video} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
