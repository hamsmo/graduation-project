import { StyleSheet } from "react-native";

export const globalStyles = StyleSheet.create({
  container: {
    flex: 1,
  },
  title: {
    fontSize: 20,
    fontWeight: "bold",
    textAlign: "center",
    textDecorationLine: "underline",
  },
  imageHeader: {
    marginVertical: 10,
    width:248,
    height:104
  },
  button: {
    height: 40,
    width: 100,
    paddingVertical: 10,
    textAlign: "center",
    backgroundColor: "#000",
    color: "#fff",
  },
  input: {
    borderBottomWidth: 1,
    borderBottomColor: "#000",
    paddingVertical: 2,
    marginVertical: 10,
    width: "90%",
  },
});
