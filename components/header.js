import React from "react";
import { StyleSheet, Text, View } from "react-native";
export default function Header() {
  return (
    <View style={styles.Header}>
      <Text style={styles.HeaderText}>Sign Langauage Translator</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  Header: {
    height: 85,
    backgroundColor: "#ccc",
    justifyContent: "center",
    alignItems: "center",
    width: "100%",
  },
  HeaderText: {
    fontSize: 25,
    fontWeight: "bold",
    marginTop: 15,
  },
});
